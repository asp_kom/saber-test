#include <stdio.h>
#include <string>

void intToBitStr(int n)
{
	std::string str;
	int bitsCount = sizeof(n) * 8;
	str.resize(bitsCount);
	for (int i = 0; i < bitsCount; ++i)
		str[bitsCount-i-1] = '0' + ((n >> i) & 0x01);
	printf("%d = %s\n", n, str.c_str());
}

void main(int argc, char* argv[])
{
	std::string buffer;
	intToBitStr(5);
	intToBitStr(-5);
	intToBitStr(4815);
	intToBitStr(-1);
}