#include <stdlib.h>
#include "List/List.h"
#include <fstream>


using namespace std;
void main(int argc, char* argv[])
{

	ofstream myOutFile;
	myOutFile.open("listData.bin", ios::out | ios::binary);
	List lst;
	lst.printDataForward();

	lst.Serialize(myOutFile);
	printf("After serialization:\n");
	lst.printDataForward();
	myOutFile.close();
	lst.clear();

	ifstream myInFile;
	myInFile.open("listData.bin", ios::in | ios::binary);
	lst.Deserialize(myInFile);
	printf("After deserialization:\n");
	lst.printDataForward();
	lst.printDataBackward();
	myInFile.close();
}

