#include "List.h"
#include <iostream>

void intToBitStr(unsigned char n, std::string &str_out)
{
	int bitsCount = sizeof(n) * 8;
	str_out.resize(bitsCount);
	for (int i = 0; i < bitsCount; ++i)
		str_out[bitsCount-i-1] = '0' + ((n >> i) & 0x01);
}

List::List( int nodesCount /*= 5*/ )
{
	if (nodesCount < 1)
		return;
	//Creating first element
	ListNode *node;
	node = new ListNode;
	node->next = 0;
	node->prev = 0;
	head = node;
	tail = node;
	node->rand = node;
	//add data to node
	intToBitStr(0, node->data);
	
	if (nodesCount < 2)
		return;
	//Temporal vector for pointers to nodes
	ListNode** pNodesVector = new ListNode*[nodesCount];
	pNodesVector[0] = head;

	//create nodes
	for (int i = 1; i < nodesCount; i++)
	{
		node = new ListNode;
		node->next = 0;
		node->prev = tail;
		tail->next = node;
		tail = node;
		intToBitStr(i, tail->data);
		pNodesVector[i] = node;
	}

	count = nodesCount;

	//assign random pointers to ListNode.rand
	node = head;
	for (int i = 0; i < count; i++)
	{
		int n = rand() % count;
		node->rand = pNodesVector[n];
		node = node->next;
	}
	delete []pNodesVector;
}


void List::clear()
{
	ListNode *node;
	node = head;
	while (node != tail)
	{
		ListNode *nodePrev = node;
		node = node->next;
		delete nodePrev;
	}
	delete node;
	count = 0;
}


List::~List()
{
	clear();
}


void List::printDataForward()
{
	printf("Printing data from head to tail:\n");
	ListNode *node = head;
	char *strBuffer = new char[sizeof(int)*8+1];
	for (int i = 0; i < count; ++i)
	{
		itoa(i, strBuffer, 10);
		printf("Node #%s\n", strBuffer);
		printf("  data:%s\n", node->data.c_str());
		printf("  rand node data:%s\n", node->rand->data.c_str());
		node = node->next;
	}
	delete [] strBuffer;
}

void List::printDataBackward()
{
	printf("Printing data from tail to head:\n");
	ListNode *node = tail;
	char *strBuffer = new char[sizeof(int)*8+1];
	for (int i = 0; i < count; ++i)
	{
		itoa(count-i-1, strBuffer, 10);
		printf("Node #%s\n", strBuffer);
		printf("  data:%s\n", node->data.c_str());
		printf("  rand node data:%s\n", node->rand->data.c_str());
		node = node->prev;
	}
	delete [] strBuffer;
}

int List::getIDbyPTR( ListNode *node )
{
	int id = 0, i = 1;
	ListNode *nodeBuf = head;
	while ((i < count) && (nodeBuf != node))
	{
		nodeBuf = nodeBuf->next;
		++i;
	}
	if (nodeBuf == node)
		id = i-1;
	return id;
}

void List::Serialize( std::ostream & stream )
{
	//write count of nodes
	stream.write((const char*)(&count), sizeof(count));
	//write nodes data
	ListNode *node = head;
	for (int i = 0; i < count; i++)
	{
		int len = node->data.size();
		stream.write((const char*)(&len), sizeof(len));
		stream.write((const char*)(node->data.c_str()),node->data.size());
		int id = getIDbyPTR(node->rand);
		stream.write((const char*)&id,sizeof(id));
		node = node->next;
	}
}

void List::Deserialize( std::istream & stream )
{
	ListNode *node;
	//read count of nodes
	stream.read((char*)(&count), sizeof(count));
	if (count < 1)
		return;

	node = new ListNode;
	int len;
	//read data length
	stream.read((char*)(&len), sizeof(len));
	node->data.resize(len);
	//read data
	stream.read((char*)(&node->data[0]), len);
	int id = 0;
	//read ListNode.rand's id. Later we'll get pointer to node from this id
	stream.read((char*)(&id), sizeof(id));
	node->rand = node;
	head = node;
	tail = node;
	node->next = 0;
	node->prev = 0;

	if (count < 2)
		return;

	//array to store IDs
	int *idArray = new int[count];
	idArray[0] = id;
	for (int i = 1; i < count; ++i)
	{
		node = new ListNode;
		stream.read((char*)(&len), sizeof(len));
		node->data.resize(len);
		stream.read((char*)(&node->data[0]), len);
		stream.read((char*)(&id), sizeof(id));
		idArray[i] = id;

		node->next = 0;
		node->prev = tail;
		tail->next = node;
		tail = node;
	}

	node = head;
	//assign pointers to ListNode.rand by ID of node
	for (int i = 0; i < count; ++i)
	{
		node->rand = getPTRbyID(idArray[i]);
		node = node->next;
	}
	delete []idArray;
}

ListNode* List::getPTRbyID( int id )
{
	ListNode* node = head;
	id = id % count;
	while (id > 0)
	{
		node = node->next;
		--id;
	}
	return node;
}
