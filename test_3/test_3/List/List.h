#ifndef LIST_H_
#define LIST_H_

#include <ostream>
#include <string>

struct ListNode {
	ListNode * 	prev;
	ListNode * 	next;
	ListNode * 	rand; // ��������� �� ������������ ������� ������� ������
	std::string 	data;
};

class List {
public:
	List(int nodesCount = 5);
	~List();
	void printDataForward(); //print nodes data from head to tail
	void printDataBackward();//print nodes data from tail to head
	void clear();

	void Serialize(std::ostream & stream);	// ���������� � ����
	void Deserialize(std::istream & stream);	// �������� �� �����
private:
	ListNode * 	head;
	ListNode * 	tail;
	int 		count;

	//get ID of node by it's pointer
	int getIDbyPTR(ListNode *node);
	//get node's pointer by it's id
	ListNode* getPTRbyID(int id);
};


#endif
