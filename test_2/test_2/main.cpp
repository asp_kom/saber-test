#include <stdio.h>
#include <string.h>

void RemoveDups(char *pStr)
{
	//getting length
	int len = strlen(pStr);
	//exit if string is too small
	if (len < 2)
		return;
	//reserve memory for buffer
	char *strBuf = new char[len+1];
	//initialize counters
	int iFrom = 1;
	int iTo = 1;
	//copy first symbol
	strBuf[0] = pStr[0];
	//copy symbol from source if it differs from last symbol in buffer
	while (iFrom < len)
	{
		if (pStr[iFrom] != strBuf[iTo-1])
		{
			strBuf[iTo++] = pStr[iFrom];
		}
		++iFrom;
	}
	//copy result from buffer to original destination
	memcpy(pStr, strBuf, iTo);
	pStr[iTo] = '\0';
	//free reserved memory
	delete [] strBuf;
}

void main(int argc, char* argv[])
{
	char str1[] = "dfgghjjJjreiurhgdaDDfgrrteeee";
	char str2[] = "AABCDDDEFFG";
	printf("Before: %s\n", str1);
	RemoveDups(str1);
	printf("After: %s\n\n",str1);
	printf("Before: %s\n", str2);
	RemoveDups(str2);
	printf("After: %s\n",str2);
}
